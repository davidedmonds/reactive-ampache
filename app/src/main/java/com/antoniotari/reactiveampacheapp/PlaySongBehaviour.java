package com.antoniotari.reactiveampacheapp;

import android.content.Context;

import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.managers.ConnectionManager;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PreferencesManager;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;
import com.facebook.network.connectionclass.ConnectionQuality;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by antonio on 31/10/17.
 */
public class PlaySongBehaviour {

    private List<Song> mSongList;
    private Song mSong;

    public PlaySongBehaviour(Song song, List<Song> songList) {
        mSong = song;
        mSongList = songList;
    }

    private String getCachedSongPath(Context context, Song song) {
        return AmpacheCache.INSTANCE.getCachedSongPath(context, song);
    }

    private void cacheSong(Context context) {
        new ConnectionManager().getConnectionQuality()
                .doOnNext(Log::blu)
                .flatMap(new Func1<ConnectionQuality, Observable<Song>>() {
                    @Override
                    public Observable<Song> call(final ConnectionQuality connectionQuality) {
                        if (connectionQuality == ConnectionQuality.EXCELLENT ||
                                connectionQuality == ConnectionQuality.GOOD||
                                connectionQuality == ConnectionQuality.MODERATE) {
                            return Observable.from(PlayManager.INSTANCE.getCurrentPlaylist().subList(PlayManager.INSTANCE.getCurrentPlaylist().indexOf(mSong),PlayManager.INSTANCE.getCurrentPlaylist().size()));
                        }
                        return Observable.from(new ArrayList<Song>());
                    }
                })
                .filter(song1 -> getCachedSongPath(context,song1)==null)
                .flatMap(songFromList -> AmpacheCache.INSTANCE.downloadSong(context,songFromList))
                .subscribe(Log::blu, Log::error);
    }

    public void playSong(Context context) {
        //            String cachePath = getCachedSongPath(view.getContext(), mSong);
//            String songUrl = cachePath == null ? mSong.getUrl() : cachePath;
//            AudioSister.getInstance().playNew(songUrl, mSong.getArtist().getName() + " - " + mSong.getTitle());
        PlayManager.INSTANCE.playNew(mSong, true);
        PlayManager.INSTANCE.setSongs(mSongList);
        PlayManager.INSTANCE.setCurrentSong(mSong);

        if (PreferencesManager.INSTANCE.isCacheEnabled()) {
            cacheSong(context);
        }
    }
}
