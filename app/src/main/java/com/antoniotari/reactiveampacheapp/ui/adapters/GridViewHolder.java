/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.antoniotari.android.lastfm.LastFm;
import com.antoniotari.android.lastfm.LastFmAlbum;
import com.antoniotari.android.lastfm.LastFmBase;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.api.AmpacheSession;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.utils.LastFmCache;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antoniotari on 2016-05-24.
 */
public class GridViewHolder extends RecyclerView.ViewHolder {

    final TextView primaryTextView;
    final TextView secondaryLeftTextView;
    final TextView secondaryRightTextView;
    final ImageView imageView;
    final CardView mainCardView;
    final ImageButton playAlbumButton;

    private Subscription lastFmSubscription;
    private LastFmBase mLastFmBase;

    GridViewHolder(View v) {
        super(v);
        primaryTextView = (TextView) v.findViewById(R.id.artistName);
        secondaryLeftTextView = (TextView) v.findViewById(R.id.artistAlbums);
        secondaryRightTextView = (TextView) v.findViewById(R.id.artistSongs);
        imageView = (ImageView) v.findViewById(R.id.artistImage);
        mainCardView  = (CardView) v.findViewById(R.id.mainCardView);
        playAlbumButton = (ImageButton) v.findViewById(R.id.playAlbumButton);
        playAlbumButton.setVisibility(View.GONE);
    }

    void setOnPlayAlbumClickListener(View.OnClickListener onPlayAlbumClickListener) {
        playAlbumButton.setOnClickListener(onPlayAlbumClickListener);
        playAlbumButton.setVisibility(View.VISIBLE);
    }

    public LastFmBase getLastFmInfo() {
        return mLastFmBase;
    }

    public void loadLastFmImage(String artistName, String albumName) {
        if (lastFmSubscription != null && !lastFmSubscription.isUnsubscribed()) {
            lastFmSubscription.unsubscribe();
        }

        imageView.setImageBitmap(null);
        lastFmSubscription = Observable.create((final Subscriber<? super LastFmBase> subscriber) -> {
            try {
                LastFmBase lastFmBase = null;
                // if album name is null we're looking for an artist
                if (albumName == null && artistName != null) {
                    // check the cache first, if not in the cache make the http request
                    lastFmBase = LastFmCache.INSTANCE.getFromCache(artistName);
                    if (lastFmBase == null) {
                        lastFmBase = LastFm.INSTANCE.getArtistImage(artistName);
                        LastFmCache.INSTANCE.putInCache(artistName, lastFmBase);
                    }
                } else if (albumName != null && artistName != null) {
                    // check the cache first, if not in the cache make the http request
                    final String key = artistName + albumName;
                    lastFmBase = LastFmCache.INSTANCE.getFromCache(key);
                    if (lastFmBase == null) {
                        lastFmBase = LastFm.INSTANCE.getAlbumInfo(artistName, albumName);
                        LastFmCache.INSTANCE.putInCache(key, lastFmBase);
                    }
                }

                if (lastFmBase == null) throw new NullPointerException("last fm response is null");

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(lastFmBase);
                    subscriber.onCompleted();
                }
            } catch (Exception e) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(e);
                }
            }
        })
                .retry(4)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).retry(4)
                .subscribe(lastFmBase -> {
                            imageView.setImageBitmap(null);
                            mLastFmBase = lastFmBase;
                            final String imageUrl;
                            if (lastFmBase instanceof LastFmAlbum) {
                                imageUrl = lastFmBase.getImages().getMedium();
                            } else {
                                imageUrl = lastFmBase.getImages().getAverageAvailable();
                            }
                            loadImage(imageUrl);
                        },
                        throwable -> {
                        }
                );
    }

    public void loadImage(String imageUrl) {
        // Replace the auth=xxxx with the current auth token
        String auth = AmpacheSession.INSTANCE.getHandshakeResponse().getAuth();
        imageUrl = imageUrl.replaceAll("auth=[0-9a-fA-F]{32}", "auth="+ auth);
        Utils.loadImage(imageUrl, imageView);
    }
}