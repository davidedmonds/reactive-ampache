package com.antoniotari.reactiveampacheapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;

import com.antoniotari.reactiveampache.models.AmpacheModel;

import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;

/**
 * Created by antoniotari on 2017-03-13.
 */

public class ColorGroupSectionTitleIndicator extends SectionTitleIndicator<AmpacheModel> {

    public ColorGroupSectionTitleIndicator(Context context) {
        super(context);
    }

    public ColorGroupSectionTitleIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorGroupSectionTitleIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSection(AmpacheModel ampacheModel) {
        // Example of using a single character
        setTitleText(ampacheModel.getName()); //colorGroup.getName().charAt(0) + "");

        // Example of using a longer string
        // setTitleText(colorGroup.getName());
        //setIndicatorTextColor(colorGroup.getAsColor());
    }
}