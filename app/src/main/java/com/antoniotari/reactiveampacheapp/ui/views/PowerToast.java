package com.antoniotari.reactiveampacheapp.ui.views;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antoniotari.reactiveampacheapp.R;

/**
 * Created by antoniotari on 2016-10-13.
 */

public class PowerToast extends Toast {
    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application} or {@link Activity} object.
     */
    public PowerToast(final Context context, final int message) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.toast_layout, null);//, (ViewGroup) findViewById(R.id.toast_layout_root));

        ImageView image = (ImageView) layout.findViewById(R.id.toastImage);
        image.setImageResource(R.drawable.ic_launcher);
        TextView text = (TextView) layout.findViewById(R.id.toastText);
        text.setText(message);

        setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        setDuration(Toast.LENGTH_SHORT);
        setView(layout);
    }
}
